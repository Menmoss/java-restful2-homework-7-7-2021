package com.shop.demo.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.shop.demo.exception.InputFieldException;
import com.shop.demo.model.Manager;
import com.shop.demo.model.Sale;
import com.shop.demo.model.Shop;
import com.shop.demo.repo.ManagerRepository;
import com.shop.demo.repo.SaleRepository;
import com.shop.demo.repo.ShopRepository;

@Controller // This means that this class is a Controller
//@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)

public class MainController {
	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private SaleRepository saleRepo;
	@Autowired
	private ManagerRepository managerRepo;

@PostMapping(path = "/shop/add", consumes = { MediaType.APPLICATION_JSON_VALUE }) // Map ONLY POST Requests
	public @ResponseBody String addNewShop(@RequestBody Shop shop) {
Set<Sale> sale = new HashSet<Sale>();

		Sale sale1 = new Sale();
		sale1.setName("Computer");
		sale1.setCost(10000);
		sale.add(sale1);

		Sale sale2 = new Sale();
		sale2.setName("Mobile");
		sale2.setCost(100000);
		sale.add(sale2);

		shopRepository.save(shop);
		sale1.setShop(shop);
		sale2.setShop(shop);
		saleRepo.save(sale1);
		saleRepo.save(sale2);
		shop.setSale(sale);

		shopRepository.save(shop);

		return "Add shop : " + shop.getShop();
	}

	@GetMapping(path = "/shop/all")
	public @ResponseBody Iterable<Shop> getAllShop() {

		return shopRepository.findAll();
	}

	@PutMapping(path = "/shop/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editShop(@RequestBody Shop shop) {

		shop.setStaff(shop.getStaff());
		shop.setComputer(shop.getComputer());
		shop.setManager(shop.getManager());
		shopRepository.save(shop);

		return "Saved";
	}

	@DeleteMapping("/shop/deleteShop/{shop_id}")
	@ResponseBody
	String deleteUser(@PathVariable("shop_id") int shopId) {

		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}

		return "Delete user : " + shopId;
	}

	// ---manager----
	@GetMapping(path = "/manager/all")
	public @ResponseBody Iterable<Manager> getAllManager() {

		return managerRepo.findAll();
	}

	@PutMapping(path = "/manager/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editManager(@RequestBody Manager manager) {

		manager.setName(manager.getName());

		managerRepo.save(manager);

		return "Saved";
	}

	@DeleteMapping("/manager/delete/{manager_id}")
	@ResponseBody
	String delete(@PathVariable("manager_id") int managerId) {

		if (!managerRepo.findById(managerId).isEmpty()) {
			managerRepo.deleteById(managerId);
		}

		return "Delete user : " + managerId;
	}

	@PostMapping(path = "/manager/add", consumes = { MediaType.APPLICATION_JSON_VALUE }) // Map ONLY POST Requests
	public @ResponseBody String addNewManager(@RequestBody Manager manager) {
		managerRepo.save(manager);

		return "Add shop : " + manager;
	}
}

